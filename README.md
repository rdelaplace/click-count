# Click Count application

[![Build Status][1]][2]
[![coverage report][3]][4]
[![License][5]](LICENSE)

## Setup with Puppet

Add the following code to the Puppet manifest.
It requires puppetlabs/docker.

### Production environment

```puppet
docker::image { 'registry.gitlab.com/rdelaplace/click-count':
  ensure    => 'present',
  image_tag => 'master',
  notify    => Exec['click-count stop']
}
exec { 'click-count stop':
  command     => 'docker stop click-count-production',
  refreshonly => true
}
docker::run { 'click-count':
  image            => 'registry.gitlab.com/rdelaplace/click-count:master',
  detach           => true,
  ports            => ['127.0.0.1:8080:8080'],
  pull_on_start    => true,
  extra_parameters => [
    '--name click-count-production',
    '--env REDIS_SERVER=<PRODUCTION_REDIS>'
  ]
}
```

### Staging environment

```puppet
docker::image { 'registry.gitlab.com/rdelaplace/click-count':
  ensure    => 'present',
  image_tag => 'staging',
  notify    => Exec['click-count stop']
}
exec { 'click-count stop':
  command     => 'docker stop click-count-staging',
  refreshonly => true
}
docker::run { 'click-count':
  image            => 'registry.gitlab.com/rdelaplace/click-count:staging',
  detach           => true,
  ports            => ['127.0.0.1:8080:8080'],
  pull_on_start    => true,
  extra_parameters => [
    '--name click-count-staging',
    '--env REDIS_SERVER=<STAGING_REDIS>'
  ]
}
```

[1]: https://gitlab.com/rdelaplace/click-count/badges/master/build.svg
[2]: https://gitlab.com/rdelaplace/click-count/commits/master
[3]: https://gitlab.com/rdelaplace/click-count/badges/master/coverage.svg
[4]: https://rdelaplace.gitlab.io/click-count/
[5]: https://img.shields.io/badge/license-Apache-blue.svg
