package fr.xebia.clickcount;

import javax.inject.Singleton;

@Singleton
public class Configuration {

    public final String redisHost;
    public final int redisPort;
    public final int redisConnectionTimeout;  //milliseconds

    public Configuration() {
        String env = System.getenv("REDIS_SERVER");
        redisHost = !env.isEmpty() ? env : "redis";
        redisPort = 6379;
        redisConnectionTimeout = 2000;
    }
}
