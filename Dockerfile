FROM tomcat:8
MAINTAINER Richard Delaplace "rdelaplace@yueyehua.net"
ENV REDIS_SERVER=""
COPY target/clickCount.war /usr/local/tomcat/webapps/clickCount.war
